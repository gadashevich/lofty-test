@extends('layouts.layout')

@section('title','Main page tests')

@section('content')
    <h1>Tests list</h1>
    @foreach($tests as $test)
        <p><a href="/tests/{{ $test->id }}">{{ $test->name_one }} : <b>{{ $test->name_two }}</b></a></p>
    @endforeach
@endsection
