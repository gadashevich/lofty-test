@extends('layouts.layout')

@section('title','Show test')

@section('content')

    <h1>Show test {{ $test->id }}</h1>
    @if($test->tasks->count())
        <div>
            @foreach($test->tasks as $task)
                <p>
                    <form method="post" action="/tasks/{{ $task->id }}">
                        @method('PATCH')
                        @csrf
                        <label for="completed">
                            <input {{ $task->completed ? 'checked' : '' }} type="checkbox" name="completed" onchange="this.form.submit()">
                            {{ $task->content }}
                        </label>
                    </form>
                </p>
            @endforeach
        </div>
    @endif
    <p><h2>name: {{ $test->name_one }}</h2></p>
    <p>descr: {{ $test->description }}</p>

    <form method="post" action="/tests/{{ $test->id }}/tasks">
        @csrf
        <div>
            <input type="text" class="input" name="content" id="name_one" value="{{ old('content') }}"/>
        </div>
        {{--<div>--}}
            {{--<input type="text" class="input" name="name_two" id="name_two" value="{{ old('name_two') }}" />--}}
        {{--</div>--}}
        <div><button type="submit">Create Task</button></div>

        @include('errors')

    </form>

@endsection
