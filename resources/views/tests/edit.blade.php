@extends('layouts.layout')

@section('title','Edit test')

@section('content')
    <h1>Edit test</h1>
    <form method="POST" action="/tests/{{ $test->id }}">
        @method('PATCH')
        @csrf
        {{--{{ method_field('PATCH') }}--}}
        {{--{{ csrf_field() }}--}}
        <div>
            <input type="text" name="name_one" id="name_one" value="{{ $test->name_one }}" required/>
        </div>
        <div>
            <input type="text" name="name_two" id="name_two" value="{{ $test->name_two }}" required/>
        </div>
        <div>
            <textarea name="description" id="description" required>{{ $test->description }}</textarea>
        </div>
        <div><button type="submit">Update</button></div>
    </form>
    <form method="POST" action="/tests/{{ $test->id }}">
        @method('DELETE')
        @csrf
        {{--{{ method_field('DELETE') }}--}}
        {{--{{ csrf_field() }}--}}
        <div><button type="submit">Delete</button></div>
    </form>
@endsection
