@extends('layouts.layout')

@section('title','Create tests')

@section('content')
    <h1>Create new test</h1>

    @include('errors')

    <form method="POST" action="/tests">
        {{--{{ csrf_field() }}--}}
        @csrf
        <div>
            <input type="text" class="input {{ $errors->has('name_one') ? 'err' : ''}}" name="name_one" id="name_one" value="{{ old('name_one') }}"/>
            {{--<input type="text" name="name_one" id="name_one" required/>--}}
        </div>
        <div>
            {{--<input type="text" name="name_two" id="name_two" required/>--}}
            <input type="text" class="input {{ $errors->has('name_two') ? 'err' : ''}}" name="name_two" id="name_two" value="{{ old('name_two') }}" />
        </div>
        <div>
            <textarea name="description" class="textarea {{ $errors->has('textarea') ? 'err' : ''}}" id="description" required>{{ old('description') }}</textarea>
        </div>
        <div><button type="submit">Create</button></div>
    </form>
@endsection
