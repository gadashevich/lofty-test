<?php

use Illuminate\Database\Seeder;

use App\Models\Movie;

class ImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = date('Y-m-d H:i:s');
        $movies = Movie::all()->where('active',1);
        foreach ($movies as $movie){
            $count = rand(3,5);
            for ($i=0;$i<$count;$i++) {
                $data = [
                    'movie_id' => $movie->id,
                    'path' => 'path/to/file',
                    'title' => 'test',
                    'created_at' => $now,
                    'updated_at' => $now,
                ];
                DB::table('images')->insert($data);
            }
        }
    }
}
