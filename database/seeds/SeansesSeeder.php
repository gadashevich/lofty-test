<?php

use Illuminate\Database\Seeder;

use App\Models\Movie;
use App\Models\Hall;

class SeansesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $now = date('Y-m-d H:i:s');

        $halls = Hall::all();

        foreach ($halls as $hall) {

            $startDate = date('Y-m-d') . ' 00:00';
            $dateTime = new DateTime($startDate);

            for ($i = 0; $i < 7; $i++) {

                $totalDurationDay = 0;

                $startDay = $dateTime->format('Y-m-d');
                $startDay = new DateTime($startDay . ' 00:00');

                while ($totalDurationDay < 24 * 60) {
                    $breakDuration = rand(60, 70);
                    $movie = DB::table("movies")
                        ->select(["movies.id", "movies.duration"])
                        ->where('active', 1)
                        ->orderBy(DB::raw('RAND()'))
                        ->take(1)
                        ->first();

                    $totalDurationDay += $movie->duration + $breakDuration;
                    $currentMinute = $totalDurationDay;

                    if ($currentMinute < 24 * 60) {

                        $start = $startDay->format('Y-m-d H:i:s');
                        $startDay->modify('+' . ($movie->duration) . ' minutes');
                        $end = $startDay->format('Y-m-d H:i:s');
                        $evning = $currentMinute > (20 * 60) ? 1 : 0;

                        $data = [
                            'movie_id' => $movie->id,
                            'hall_id' => $hall->id,
                            'start' => $start,
                            'end' => $end,
                            'evning' => $evning,
                            'done' => strtotime($start) > strtotime($now) ? 0 : 1,
                            'created_at' => $now,
                            'updated_at' => $now,
                        ];
                        $startDay->modify('+' . $breakDuration . ' minutes');
                        DB::table('seances')->insert($data);

                    }
                }

                $dateTime->modify('+1 day');

            }
        }
    }
}
