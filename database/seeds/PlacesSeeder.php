<?php

use Illuminate\Database\Seeder;

use App\Models\Hall;

class PlacesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = date('Y-m-d H:i:s');
        $halls = Hall::all();

        foreach ($halls as $hall) {
            for ($row = 1; $row <= $hall->countRows; $row++) {
                for ($place = 1; $place <= $hall->countPlacesPerRow; $place++) {
                    $data = [
                        'hall_id' => $hall->id,
                        'row' => $row,
                        'place' => $place,
                        'vip' => 0,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ];
                    DB::table('places')->insert($data);
                }
            }
        }
    }

}
