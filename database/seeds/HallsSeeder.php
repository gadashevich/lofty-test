<?php

use Illuminate\Database\Seeder;

class HallsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = date('Y-m-d H:i:s');
        $data = [
            [
                'name'  => 'hall_one',
                'countRows' => 10,
                'countPlacesPerRow' => 20,
                'price_main' => 300,
                'price_vip' => 100,
                'created_at' => $now,
                'updated_at' => $now,
            ],
            [
                'name'  => 'hall_two',
                'countRows' => 15,
                'countPlacesPerRow' => 25,
                'price_main' => 400,
                'price_vip' => 100,
                'created_at' => $now,
                'updated_at' => $now,

            ]
        ];
        DB::table('halls')->insert($data);
    }
}
