<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        factory(\App\Models\Movie::class,20)->create();
        $this->call(ImagesSeeder::class);
        $this->call(HallsSeeder::class);
        $this->call(SeansesSeeder::class);
        $this->call(PlacesSeeder::class);
    }
}
