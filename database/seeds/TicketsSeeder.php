<?php

use Illuminate\Database\Seeder;

use App\Models\Seance;
use App\Models\Place;
use App\User;
use App\Models\Ticket;

use Illuminate\Support\Facades\DB;

class TicketsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ticket::query()->truncate();

        $now = date('Y-m-d H:i:s');
        $seanse = Seance::all()->first();
        $user = User::all()->first();
        $hall = $seanse->hall;

        $limit = rand(1, $hall->countRows * $hall->countPlacesPerRow);

        $places = Place::query()
            ->where('hall_id', '=', $hall->id)
            ->inRandomOrder()
            ->limit($limit)
            ->get();

        foreach ($places as $place) {
            $data = [
                'seance_id' => $seanse->id,
                'place_id' => $place->id,
                'user_id' => $user->id,
                'price' => 300,
                'done' => 1,
                'created_at' => $now,
                'updated_at' => $now,
            ];
            DB::table('tickets')->insert($data);
        }
    }
}
