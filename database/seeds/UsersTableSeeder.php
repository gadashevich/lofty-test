<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name'  => 'test1',
                'email' => 'test1@test.te',
                'password' => bcrypt('123test'),
            ],
            [
                'name'  => 'test2',
                'email' => 'test2@test.te',
                'password' => bcrypt('123test'),
            ]
        ];
        DB::table('users')->insert($data);
    }
}
