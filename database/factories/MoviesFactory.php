<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Movie::class, function (Faker $faker) {
    $name = $faker->sentence(rand(1,4));
    $description = $faker->realText(rand(300,500));
    $duration = rand(60,3*60);

    return [
        'slug' => str_slug($name),
        'name' => $name,
        'active' => 1,
        'poster' => 'path/to/poster',
        'duration' => $duration,
        'description' => $description,
        'created_at' => $faker->dateTimeBetween('-1 month','-1 day'),
        'updated_at' => $faker->dateTimeBetween('-1 month','-1 day')
    ];
});
