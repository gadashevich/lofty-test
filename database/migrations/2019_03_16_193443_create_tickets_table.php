<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('seance_id')->unsigned();
            $table->integer('place_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->tinyInteger('done')->default(0)->index('done');

            $table->decimal('discount')->default(0);
            $table->decimal('price')->default(0);

            $table->unique(['seance_id','place_id']);
            $table->foreign('seance_id')->references('id')->on('seances')->onDelete('cascade');
            $table->foreign('place_id')->references('id')->on('places')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->index('user_id');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
