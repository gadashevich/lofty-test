<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Repositories\Cinema\EloquentCinemaRepository;
use TicketsSeeder;


class CinemaTest extends TestCase
{

    protected $seanceId = 1;

    protected $repository;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        $this->repository = new EloquentCinemaRepository();
        parent::__construct($name, $data, $dataName);
    }


    public function setUp()
    {
        parent::setUp();
        $this->seed(TicketsSeeder::class);

    }

    /**
     * проверить количество доступных мест на сеанс
     *
     * @return void
     */
    public function test_seanse_aviliable_count_places()
    {
        $placesCount = $this->repository->getSeanceAviliablePlacesCount( $this->seanceId);
        $this->assertIsInt($placesCount);
    }

    public function testExample()
    {
        $this->assertIsInt(1);
    }

}
