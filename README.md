### Установка

Для корректной установки на вашем сервере должны быть установлены PHP  7.1.3 иили выше, 
MySQL 5.6 и выше
расширение mcrypt для PHP и Composer. Для установки сделайте следующее:

Склонировать проект на локальную машину, войти в папку проекта

```
git clone https://gadashevich@bitbucket.org/gadashevich/lofty-test.git
cd prettyforms-laravel-app/
```
Установить все зависимости приложения через Composer

```
composer install
```


Настроить подключение к MySQL базе данных в файле app/config/database.php
Создать базу данных приложения, выполнив SQL-запрос в MySQL

```
CREATE DATABASE `laravel` COLLATE 'utf8_general_ci'
```
Запустить скрипт генерации таблиц БД
```
php artisan migrate:fresh --seed
```
Запустить веб-сервер
```
php artisan serve
```
Открыть страницу в браузере: http://localhost:8000/


### Проверка
1. Открыть страницу в браузере: http://localhost:8000/
2. Залогиниться под учетной записью 
Логин: test1@test.te 
Пароль: 123test
3. Перейти на страницу http://localhost:8000/schedule покажет расписание сеансов на текущие сутки в виде массива
4. Перейти на страницу http://localhost:8000/buy купит первый билет на первый сеанс
5. Запустить юнит тест в коммандной строке 

```
./vendor/bin/phpunit --filter test_seanse_aviliable_count_places CinemaTest tests/Unit/CinemaTest.php

```