<?php

namespace App\Exceptions;

use Exception;

class PaymentException extends Exception
{
    // Error codes.
    public const PAYMENT_FAILED = 7;


    public const PLACE_NOT_FREE = 3;
    public const USER_NOT_AUTH = 4;

    /**
     * Payment failed on the bank side.
     *
     * @param string $message
     * @return \App\Exceptions\PaymentException
     */
    public static function paymentFailed(string $message): self
    {
        return new self($message, self::PAYMENT_FAILED);
    }


    /**
     *  Place on Seance is not free.
     *
     * @param int $seanceId
     * @param int $placeId
     * @return \App\Exceptions\PaymentException
     */
    public static function placeNotFree(int $seanceId,int $placeId): self
    {
        return new self("Place #{$placeId} on seance #{$seanceId} is not free", self::PLACE_NOT_FREE );
    }

    /**
     *  User not auth.
     *
     * @return \App\Exceptions\PaymentException
     */
    public static function userNotAuth(): self
    {
        return new self("User not auth", self::USER_NOT_AUTH);
    }



}
