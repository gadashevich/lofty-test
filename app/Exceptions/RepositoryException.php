<?php

namespace App\Exceptions;

use Exception;

class RepositoryException extends Exception
{
    public const SEANCE_NOT_FOUND = 1;
    public const PLACE_NOT_FOUND = 2;
    public const HALL_NOT_FOUND = 5;
    public const MOVIE_NOT_FOUND = 6;


    /**
     * Seance not found.
     *
     * @param int $seanceId
     * @return \App\Exceptions\RepositoryException
     */
    public static function seanceNotFound(int $seanceId): self
    {
        return new self("Seance not found #{$seanceId}", self::SEANCE_NOT_FOUND );
    }

    /**
     * Place not found.
     *
     * @param int $placeId
     * @return \App\Exceptions\RepositoryException
     */
    public static function placeNotFound(int $placeId): self
    {
        return new self("Place not found #{$placeId}", self::PLACE_NOT_FOUND );
    }

    /**
     * Hall not found.
     *
     * @return \App\Exceptions\RepositoryException
     */
    public static function hallNotFound(): self
    {
        return new self("Hall not found ", self::HALL_NOT_FOUND );
    }


    /**
     * Hall not found.
     *
     * @return \App\Exceptions\RepositoryException
     */
    public static function movieNotFound(): self
    {
        return new self("Movie not auth", self::MOVIE_NOT_FOUND);
    }

}
