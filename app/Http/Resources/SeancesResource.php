<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;


class SeancesResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'start' => $this->start,
            'end' => $this->end,
            'hall_id' => $this->hall->id,
            'hall_name' => $this->hall->name,
            'duration' => $this->movie->duration,
            'seance_id' => $this->id,
            'movie_id' => $this->movie->id,
            'movie_name' => $this->movie->name,
            'movie_poster' => $this->movie->poster,
            'movie_images' => $this->movie->images,
            'movie_description' => $this->movie->description,
            'move_images' => $this->movie->images,
            'count_free_places' => $this->hall->places_count - $this->tickets_count,
        ];
    }
}
