<?php

namespace App\Http\Controllers;

use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use App\Repositories\Cinema\CinemaRepository;
use App\Http\Resources\SeancesResource;

use App\Servises\Payment\PaymentGetway;
use App\Servises\Payment\Payment;

class HomeController extends Controller
{
    public function index(CinemaRepository $сinemaRepository)
    {


        $dateStart = date('Y-m-d H:i:s');

        $dateEnd =  date("Y-m-d", strtotime("+1 day"));
        $hallId = null;

        $items = $сinemaRepository->findSeances($dateStart,$dateEnd,$hallId);

        //если надо вернуть в формате json
//        return SeancesResource::collection (
//            $items
//        );

        echo '<pre>';
        print_r($items->toArray());
        echo '</pre>';
        die();

    }


    public function buy(PaymentGetway $paymentGetway,CinemaRepository $сinemaRepository)
    {
        $payment = new Payment($paymentGetway,$сinemaRepository);

        $seanceId = 1;
        $placeId = 1;


        $ticket = $payment->buyTicket($seanceId,$placeId,null);
        echo '<pre>';
        print_r($ticket->toArray());
        echo '</pre>';
        die();
    }

    public function check(CinemaRepository $сinemaRepository)
    {
        $places = $сinemaRepository->getSeanceAviliablePlaces(1);
        echo '<pre>';
        print_r($places);
        echo '</pre>';
        die();

    }
}
