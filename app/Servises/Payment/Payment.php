<?php
/**
 * Created by PhpStorm.
 * User: boris
 * Date: 17.03.19
 * Time: 15:51
 */

namespace App\Servises\Payment;

use App\Models\Ticket;
use App\Models\Seance;
use App\Models\Place;

use App\Exceptions\PaymentException;
use App\Exceptions\RepositoryException;

use Illuminate\Support\Facades\DB;
use App\Repositories\Cinema\CinemaRepository;


class Payment implements PaymentInterface
{
    protected $paymentGetway;

    protected $repository;

    /**
     * Payment constructor.
     *
     * @param App\Servises\Payment $paymentGetway
     * @param App\Repositories\Cinema\CinemaRepository
     *
     */
    public function __construct(PaymentGetway $paymentGetway, CinemaRepository $repository)
    {
        $this->paymentGetway = $paymentGetway;
        $this->repository = $repository;
    }


    /**
     *  Calculate the price for a place in the cinema
     *
     * @param App\Models\Seance $seance
     * @param App\Models\Place $place
     * @return float
     *
     * @throws RepositoryException
     */
    protected function calculatePrice(Seance $seance,Place $place, float $discount = null)
    {
        if(!$place->hall)
            throw RepositoryException::hallNotFound();

        $priceBase = $place->hall->price_main;
        if($place->vip){
            $priceBase += $place->hall->price_vip;
        }
        if($seance->evning)
            $priceBase += config('sinema.price_evning');
        if($discount)
            $priceBase -= $discount;
        return (float)$priceBase;
    }

    /**
     * Buy Ticket.
     *
     * @param int $seanceId
     * @param int $placeId
     * @param float|null $discount
     * @return App\Models\Ticket
     *
     * @throws PaymentException
     * @throws RepositoryException
     *
     */
    public function buyTicket(int $seanceId,int $placeId, float $discount = null)
    {
        $seance = Seance::find($seanceId);
        if(!$seance){
            throw RepositoryException::seanceNotFound($seanceId);
        }
        $place = Place::find($placeId);
        if(!$place){
            throw RepositoryException::placeNotFound($placeId);
        }
        $check = $this->repository->checkIsFreePlace($seanceId,$placeId);

        if(!$check){
            throw PaymentException::placeNotFree($seanceId,$placeId);
        }
        $user = auth()->user();
        if(!$user){
            throw PaymentException::userNotAuth();
        }

        $movie = $seance->movie;
        if(!$movie){
            throw RepositoryException::movieNotFound();
        }

        $seance->movie()->associate($movie);

        DB::beginTransaction();

        $ticket = new Ticket();
        $ticket->seance()->associate($seance);
        $ticket->place()->associate($place);
        $ticket->user()->associate($user);
        $ticket->price = $this->calculatePrice($seance,$place,$discount);
        if($discount)
            $ticket->discount = $discount;


        $response = $this->paymentGetway->authorize([
            'clientId' => $user->id,
            'amount' => $ticket->price,
            'currency' => 810,
            'description' => 'Payment for a session in the cinema',
            'language' => config('app.locale'),
            'returnUrl' => config('app.url').'/result',

        ])->send();

        if (!$response->isSuccessful()) {
            DB::rollBack();
            throw PaymentException::paymentFailed($response->getMessage());
        }
        $ticket->save();

        $response = $this->paymentGetway->purchase([
            'ip' => request()->ip()
        ])->send();

        if (!$response->isSuccessful()) {
            DB::rollBack();
            throw PaymentException::paymentFailed($response->getMessage());
        }

        DB::commit();

        return $ticket;

    }
}