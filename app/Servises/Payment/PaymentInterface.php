<?php
/**
 * Created by PhpStorm.
 * User: boris
 * Date: 18.03.19
 * Time: 1:04
 */

namespace App\Servises\Payment;


interface PaymentInterface
{
    /**
     * Buy Ticket.
     *
     * @param int $seanceId
     * @param int $placeId
     * @param float|null $discount
     * @return
     *
     * @throws PaymentException
     *
     */
    public function buyTicket(int $seanceId,int $placeId, float $discount = null);

}