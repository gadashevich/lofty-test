<?php
/**
 * Created by PhpStorm.
 * User: boris
 * Date: 17.03.19
 * Time: 20:27
 */

namespace App\Servises\Payment;


class PaymentResponse
{
    public function isSuccessful()
    {
        return true;
    }

    public function getMessage()
    {
        return 'Payment Error';
    }
}