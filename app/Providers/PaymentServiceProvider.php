<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Servises\Payment\PaymentGetway;

class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(PaymentGetway::class, function (): PaymentGetway {
            return (new PaymentGetway());
//                ->setUserName(config('services.paymentgate.login'))
//                ->setPassword(config('services.paymentgate.password'))
//                ->setTestMode(config('services.paymentgate.test'));
        });
    }
}
