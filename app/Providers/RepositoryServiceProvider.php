<?php

namespace App\Providers;

use App\Repositories\Cinema\CinemaRepository;
use App\Repositories\Cinema\EloquentCinemaRepository;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bind concrete to Repository some contract.
     *
     * @var array
     */
    public $bindings = [
        CinemaRepository::class => EloquentCinemaRepository::class,
    ];

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(): void
    {
        foreach ($this->bindings as $abstract => $concrete) {
            $this->app->bind($abstract, $concrete);
        }
    }
}
