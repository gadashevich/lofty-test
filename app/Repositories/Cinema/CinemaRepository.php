<?php
/**
 * Created by PhpStorm.
 * User: boris
 * Date: 17.03.19
 * Time: 13:34
 */

namespace App\Repositories\Cinema;


interface CinemaRepository
{
    /**
     *  find Seances between two dates
     *
     * @param string $dateStart
     * @param string $dateEnd
     * @param int|null $hallId
     * @return mixed
     */
    public function findSeances(string $dateStart, string $dateEnd, int $hallId = null);

    /**
     *  check that the seance place is free
     *
     * @param int $seanceId
     * @param int $placeId
     * @return bool
     */
    public function checkIsFreePlace(int $seanceId,int $placeId);


    /**
     *  get Seance Aviliable Places
     *
     * @param int $seanceId
     * @return mixed
     */
    public function getSeanceAviliablePlaces(int $seanceId);


    /**
     *  get Seance Inaccessible Places
     *
     * @param int $seanceId
     * @return mixed
     */
    public function getSeanceInaccessiblePlaces(int $seanceId);


    /**
     *  get seance aviliable places count
     *
     * @param int $seanceId
     * @return mixed
     */
    public function getSeanceAviliablePlacesCount(int $seanceId);

}