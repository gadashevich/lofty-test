<?php

namespace App\Repositories\Cinema;


use App\Models\Seance;
use App\Models\Place;
use App\Models\Ticket;

use App\Exceptions\RepositoryException;


class EloquentCinemaRepository implements CinemaRepository
{

    /**
     *  check that the seance place is free
     *
     * @param int $seanceId
     * @param int $placeId
     * @return bool
     */
    public function checkIsFreePlace(int $seanceId,int $placeId)
    {
        $count = Ticket::query()
            ->where('seance_id', '=',$seanceId)
            ->where('place_id','=',$placeId)
            ->count();

        return $count ? false : true;

    }

    /**
     *  find Seances between two dates
     *
     * @param string $dateStart
     * @param string $dateEnd
     * @param int|null $hallId
     * @return mixed
     */
    public function findSeances(string $dateStart, string $dateEnd, int $hallId = null)
    {
        $columns = ['*'];
        $query  = Seance::query()
            ->with(
                array('movie' => function ($query) {
                    $query->with('images')->get(['*']);
                }
            ))
            ->with(
                array('hall' => function ($query) {
                    $query->withCount('places')->get(['id']);
                }
            ))
            ->withCount('tickets')
            ->where('start', '>=',$dateStart)
            ->where('start', '<=',$dateEnd)
            ->where('done', '=',0)
            ->where(function ($query) use ($hallId){
                if($hallId)
                    $query->where('hall_id', '=', $hallId);
            })
            ->get($columns);

        return $query;

//        $items = [];
//        foreach ($query as $item){
//            if($item->hall->places_count > $item->tickets_count)
//                $items[] = $item;
//        }
//        return $items;

    }

    /**
     *  get Seance Inaccessible Places
     *
     * @param int $seanceId
     * @return Illuminate\Database\Eloquent\Collection
     * @throws RepositoryException
     *
     *
     */
    public function getSeanceInaccessiblePlaces(int $seanceId)
    {
        $seance = Seance::find($seanceId);

        if(!$seance)
            throw RepositoryException::seanceNotFound($seanceId);

        if(!$seance->hall)
            throw RepositoryException::hallNotFound();

        $hallId = $seance->hall->id;

        return Place::whereHas('ticket', function ($query) use ($seanceId) {
            $query->where('seance_id', '=', $seanceId);
        })
            ->where('places.hall_id', '=', $hallId)
            ->get();
    }

    /**
     *  get Seance Aviliable Places
     *
     * @param int $seanceId
     * @return Illuminate\Database\Eloquent\Collection
     * @throws RepositoryException
     *
     */
    public function getSeanceAviliablePlaces(int $seanceId)
    {
        $seance = Seance::find($seanceId);
        if(!$seance)
            throw RepositoryException::seanceNotFound($seanceId);

        if(!$seance->hall)
            throw RepositoryException::hallNotFound();

        $hallId = $seance->hall->id;

        ///RAW SQL
        ///
//        echo $sql = "SELECT places.*,count(tickets.id) as count_tickets
//                FROM  places
//                LEFT JOIN tickets ON (`places`.`id` = `tickets`.`place_id` AND tickets.seance_id = $seanceId)
//                WHERE places.hall_id = $hallId
//                GROUP BY `places`.`id`
//                HAVING count_tickets = 0
//                ORDER BY places.id";
//        die();
//        $places = DB::select($sql);


        return Place::whereDoesntHave('ticket', function ($query) use ($seanceId) {
            $query->where('seance_id', '=', $seanceId);
        })
            ->where('places.hall_id', '=', $hallId)
            ->get();
    }


    /**
     * get seance aviliable places count
     *
     * @param int $seanceId
     * @return int
     * @throws RepositoryException
     *
     */
    public function getSeanceAviliablePlacesCount(int $seanceId)
    {
        $seance = Seance::find($seanceId);
        if(!$seance)
            throw RepositoryException::seanceNotFound($seanceId);

        if(!$seance->hall)
            throw RepositoryException::hallNotFound();

        $hallId = $seance->hall->id;


        return Place::whereDoesntHave('ticket', function ($query) use ($seanceId) {
            $query->where('seance_id', '=', $seanceId);
        })
            ->where('places.hall_id', '=', $hallId)
            ->count();
    }

}
