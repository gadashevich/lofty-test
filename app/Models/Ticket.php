<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use App\User;

class Ticket extends Model
{

    /**
     * Ticket associated with Seance.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function seance() : BelongsTo
    {
        return $this->BelongsTo(Seance::class);
    }



    /**
     * Ticket associated with Plase.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function place() : BelongsTo
    {
        return $this->BelongsTo(Place::class);
    }

    /**
     * Ticket associated with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }

}
