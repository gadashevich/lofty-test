<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Hall extends Model
{
    /**
     * Hall associated with Place.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function places() : HasMany
    {
        return $this->hasMany(Place::class);
    }
}
