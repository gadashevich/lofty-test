<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;


class Place extends Model
{
    /**
     *  Place associated with Hall.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function hall() : belongsTo
    {
        return $this->belongsTo(Hall::class);
    }

    /**
     *  Place associated with Ticket.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function ticket() : hasOne
    {
        return $this->hasOne(Ticket::class);
    }
}
