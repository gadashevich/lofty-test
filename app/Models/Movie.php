<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;


class Movie extends Model
{
    /**
     * Movie associated with Image.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images() : HasMany
    {
        return $this->hasMany(Image::class);
    }
}
