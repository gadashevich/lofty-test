<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;



class Seance extends Model
{
    /**
     * Seance associated with Movie.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function movie() : BelongsTo
    {
        return $this->BelongsTo(Movie::class);
    }

    /**
     * Seance associated with Hall.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hall() : BelongsTo
    {
        return $this->belongsTo(Hall::class);
    }

    /**
     * Seance associated with Tickets.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets() : HasMany
    {
        return $this->hasMany(Ticket::class);
    }
}
