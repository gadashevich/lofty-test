<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//https://laracasts.com/series/laravel-from-scratch-2018/episodes/11


//Route::resource('tests','TestsController');
//Route::patch('tasks/{task}','TestsTasksController@update');
//Route::post('tests/{test}/tasks','TestsTasksController@store');

//Route::get('/',             'PagesController@home');
//Route::get('/tests',    'TestsController@index');
//Route::get('/tests/create',    'TestsController@create');
//Route::get('/tests/{test}',    'TestsController@show');
//Route::get('/tests/{test}/edit',    'TestsController@edit');
//Route::post('/tests',    'TestsController@store');
//Route::patch('/tests/{test}','TestsController@update');
//Route::delete('/tests/{test}','TestsController@delete');

//Route::get('/', function () {
//    $data = [
//        'one',
//        'two',
//        'three'
//    ];
//    return view('welcome',[
//        'data' => $data
//    ]);
//});

//Route::get('/contact', function () {
//    return view('contact');
//});


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/schedule', 'HomeController@index');
Route::get('/buy', 'HomeController@buy');
Route::get('/check', 'HomeController@check');
